# acuerol CUSTOM
  '\\texttt':
    'prefix': 'texttt'
    'body': '\\\\texttt{${1:script text}}$0'
  'texttt':
    'prefix': '\\texttt'
    'body': '\\\\texttt{${1:script text}}$0'
  '\\textit':
    'prefix': 'textit'
    'body': '\\\\textit{${1:text italic}}$0'
  'textit':
    'prefix': '\\textit'
    'body': '\\\\textit{${1:text italic}}$0'
  '\\textbf':
    'prefix': 'textbf'
    'body': '\\\\textbf{${1:text bold}}$0'
  'textbf':
    'prefix': '\\textbf'
    'body': '\\\\textbf{${1:text bold}}$0'

   '\\path':
     'prefix': 'path'
     'body': '\\\\path{${1:path}}$0'
   'path':
     'prefix': '\\path'
     'body': '\\\\path{${1:path}}$0'

   '\\url':
     'prefix': 'url'
     'body': '\\\\url{${1:URL}}$0'
   'url':
     'prefix': '\\url'
     'body': '\\\\url{${1:URL}}$0'
    '\\href':
     'prefix': 'href'
     'body': '\\\\href{${1:href}}{${2:text}}$0'
    'href':
     'prefix': '\\href'
     'body': '\\\\href{${1:href}}{${2:text}}$0'
    '\\footnote':
      'prefix': 'footnote'
      'body': '\\\\footnote{${1:footnote}}$0'
    'footnote':
      'prefix': '\\footnote'
      'body': '\\\\footnote{${1:footnote}}$0'
    'TODO':
      'prefix': 'TODO'
      'body': '{\\\\color{blue} \\\\textbf{TODO:} $0}'
    'FIXME':
      'prefix': 'FIXME'
      'body': '{\\\\color{red} \\\\textbf{FIXME:} $0}'
    'lstlisting':
      'prefix': 'lstlisting'
      'body': '\\\\begin{lstlisting}[language={[${1:}]${2:tex}}, numbers=${3:left}]\n\t$0\n\\\\end{lstlisting}'
